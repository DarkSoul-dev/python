import math

class Circle:
    def __init__(self,radius):
        self.radius=radius
    def area(self):
        return math.pi*(self.radius**2)
    def parameter(self):
        return 2*math.pi*self.radius

radius = int(input("enter radius: "))

obj=Circle(radius)
print("area of circle is: ",obj.area())
print("parameter of circle is: ",obj.parameter())
