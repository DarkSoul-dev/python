<?php
$x =5; //global scope

function myTest(){
    //using X inside this function will generate an error
    echo "<p>Variable x inside function is $x</p>";

}
myTest();
echo "<p>Variable x inside function is $x</p>";
?>
